import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.*;

public class TestUnit {

	private Polinom p1 = new Polinom(0);
	private Polinom p2 = new Polinom(0);
	private Polinom p3 = new Polinom(0);
	private String rezultat = new String("");
	private String expected = new String("");
	private Controller c = new Controller();

	@Before
	public void initializare() { // initializare inainte de fiecare test

		p1 = c.prelucrare("12x^4-6x^3+2x^1+7x^0");
		p2 = c.prelucrare("3x^2+2x^0");

		p1.completeaza_inferior();
		p2.completeaza_inferior();
		p1.completeaza_superior(p2);
	}

	public void show() {
		System.out.println("Rezultat operatie: " + rezultat.toString());
		System.out.println("Rezultat asteptat: " + expected.toString());
	}

	@Test
	public void testCitire() {

		Polinom p11 = new Polinom(0);
		Polinom p12 = new Polinom(0);

		String rezultat11 = new String("");
		String expected12 = new String("");

		p11.adaugaMonominPolinom(new Monom(2, 2));
		p11.adaugaMonominPolinom(new Monom(0, -3));
		rezultat11 = p11.showPolinom();

		p12 = c.prelucrare("2x^2-3x^0");
		expected12 = p12.showPolinom();

		assertEquals(rezultat11, expected12);

		System.out.println("Citire: ");
		System.out.println(rezultat11.toString());
		System.out.println(expected12.toString());
	}

	@Test
	public void testAdunare() {

		initializare();

		p3 = p1.adunare(p2);
		p3.sorteaza();
		p3 = p3.aranjeaza();

		rezultat = p3.showPolinom();
		expected = "+ 12*x^4 - 6*x^3 + 3*x^2 + 2*x^1 + 9*x^0 ";

		assertEquals(rezultat, expected);
		System.out.println("Adunare: ");
		show();

	}

	@Test
	public void testScadere() {

		initializare();

		p3 = p1.scadere(p2);
		p3.sorteaza();
		p3 = p3.aranjeaza();
		rezultat = p3.showPolinom();

		expected = "+ 12*x^4 - 6*x^3 - 3*x^2 + 2*x^1 + 5*x^0 ";

		assertEquals(rezultat, expected);
		System.out.println("Scadere: ");
		show();

	}

	@Test
	public void testIntegrare() {

		initializare();

		p3 = p1.integrare();

		rezultat = p3.showPolinom();
		expected = "+ 2.40*x^5 - 1.50*x^4 + 1.00*x^2 + 7.00*x^1 ";

		assertEquals(rezultat, expected);
		System.out.println("Integrare: ");
		show();
	}

	@Test
	public void testDerivare() {

		initializare();

		p3 = p1.derivare();

		rezultat = p3.showPolinom();
		expected = "+ 48*x^3 - 18*x^2 + 2*x^0 ";

		assertEquals(rezultat, expected);
		System.out.println("Derivare: ");
		show();
	}

	@Test
	public void testInmultire() {

		initializare();

		p3 = p1.inmultire(p2);
		p3 = p3.aranjeaza();

		rezultat = p3.showPolinom();
		expected = "+ 36*x^6 - 18*x^5 + 24*x^4 - 6*x^3 + 21*x^2 + 4*x^1 + 14*x^0 ";
		assertEquals(rezultat, expected);
		System.out.println("Inmultire: ");
		show();
	}

	@Test
	public void testImpartire() {

		initializare();

		String rezultat1 = new String("");
		String expected1 = new String("");

		try {

			ArrayList<Polinom> a = p1.impartire(p2);
			rezultat = rezultat + a.get(0).showPolinom();
			rezultat1 = rezultat1 + a.get(1).showPolinom();

		} catch (Exception exception) {
			rezultat = exception.getMessage();
			rezultat1 = "";

		}

		expected = "+ 4*x^2 - 2*x^1 - 2*x^0 ";
		expected1 = "- 2*x^2 + 6*x^1 + 11*x^0 ";

		assertEquals(rezultat, expected);
		assertEquals(rezultat1, expected1);
		System.out.println("Impartire: ");
		show();
		System.out.println(rezultat1.toString());
		System.out.println(expected1.toString());
	}
}