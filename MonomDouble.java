
public class MonomDouble extends Number {

	public static final long serialVersionUID = 1L;
	private Number number;
	private int grad;
	private double coeficient;

	public MonomDouble() {
		this(0, 0);
	}

	public MonomDouble(Number grad, Number coeficient) {
		this.grad = grad.intValue();
		this.coeficient = coeficient.doubleValue();
	}

	public int intValue() {
		return number.intValue();
	}

	public double doubleValue() {
		return number.doubleValue();
	}

	public float floatValue() {
		return number.floatValue();
	}

	public long longValue() {
		return number.longValue();
	}

	public double getCoeficient() {

		return coeficient;
	}

	public int getGrad() {

		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}

	public String toString() { // Metoda de afisare a unui monom

		if (this.coeficient != 0) {
			if (this.coeficient >= 0) {
				return String.format("+ %.2f*x^%d ", this.coeficient, this.getGrad());

			} else if (this.coeficient < 0) {
				return String.format("- %.2f*x^%d ", -this.coeficient, this.getGrad());

			}
		}
		return "";
	}

	public int getGradMax(MonomDouble p) { // Metoda care returneaza gradul
											// maxim al unui Monom

		if (p.getGrad() >= this.getGrad())
			return p.getGrad();
		else
			return this.getGrad();
	}

	public MonomDouble impartire(Monom p) { // Metoda care returneaza rezultatul
											// impartirii a 2 monoame

		MonomDouble m = this;
		MonomDouble rezultat = new MonomDouble(m.getGrad() - p.getGrad(), 0);
		rezultat.setCoeficient(m.getCoeficient() * (1 / p.getCoeficient()));
		rezultat.setGrad(m.getGrad() - p.getGrad());

		return rezultat;
	}

}
