import java.util.ArrayList;
import java.util.Comparator;

public class Polinom {

	private int gradPolinom;
	private ArrayList<Monom> monoame = new ArrayList<Monom>();
	private ArrayList<MonomDouble> monoame_double = new ArrayList<MonomDouble>();

	public Polinom() { // Constructor Polinom de gradul 0
		this.gradPolinom = 0;
	}

	public Polinom(int gradPolinom) { // Constructor Polinom avand un grad dat

		this.gradPolinom = gradPolinom;
	}

	public void adaugaMonominPolinom(Monom m) { // Metoda de adaugare a
												// monoamelor intregi in polinom

		monoame.add(m);
		gradPolinom = this.getGrad();
	}

	public void adaugaMonominPolinomDouble(MonomDouble m) {// Metoda de adaugare
															// a monoamelor
															// double in polinom

		monoame_double.add(m);
		gradPolinom = this.getGrad();

	}

	public void completeaza_superior(Polinom p) { // Metoda care compara gradele
													// a 2 polinoame, si il
													// completeaza cu monoame de
													// coeficient 0 pe cel cu
													// grad mai mic
		Polinom m = this;
		if (m.gradPolinom > p.gradPolinom) {
			for (int i = m.gradPolinom; i > p.gradPolinom; i--) {

				Monom m1 = new Monom(i, 0);
				p.adaugaMonominPolinom(m1);
			}
		}

		if (p.gradPolinom > m.gradPolinom) {
			for (int i = p.gradPolinom; i > m.gradPolinom; i--) {

				Monom m1 = new Monom(i, 0);
				m.adaugaMonominPolinom(m1);
			}
		}
	}

	public void completeaza_inferior() {// Metoda care adauga monoame de
										// coeficient 0 intr-un Polinom de un
										// anumit grad

		int[] vector = new int[1000];

		for (Monom m1 : this.monoame) {
			vector[m1.getGrad()] = 1;

		}

		for (int i = 0; i < this.getGrad(); i++) {

			if (vector[i] != 1) {
				Monom m2 = new Monom(i, 0);
				this.adaugaMonominPolinom(m2);
			}
		}
	}

	public Polinom adunare(Polinom p) { // Metoda de adunare a 2 polinoame

		Polinom m = this;
		Polinom adunate = new Polinom(m.getGradMax(p));

		for (Monom m1 : m.monoame) {
			for (Monom m2 : p.monoame) {

				int y = m1.getGradMax(m2);
				Monom monom_nou = new Monom(y, 0);

				if (m1.getGrad() == m2.getGrad()) {
					monom_nou.setCoeficient(m1.getCoeficient() + m2.getCoeficient());

					adunate.adaugaMonominPolinom(monom_nou);
				}
			}
		}
		adunate = adunate.aranjeaza();
		adunate.sorteaza();
		return adunate;
	}

	public Polinom scadere(Polinom p) { // Metoda de scadere a 2 polinoame
		Polinom m = this;

		Polinom diferenta = new Polinom(m.getGradMax(p));

		for (Monom m1 : m.monoame) {

			for (Monom m2 : p.monoame) {

				int y = m1.getGradMax(m2);
				Monom monom_nou = new Monom(y, 0);

				if (m1.getGrad() == m2.getGrad()) {
					monom_nou.setCoeficient(m1.getCoeficient() - m2.getCoeficient());

					diferenta.adaugaMonominPolinom(monom_nou);
				}
			}
		}
		diferenta = diferenta.aranjeaza();
		diferenta.sorteaza();
		return diferenta;
	}

	public Polinom derivare() { // Metoda de derivare a 2 polinoame

		Polinom m = this;
		Polinom derivat = new Polinom(m.getGrad() - 1);

		for (Monom m1 : m.monoame) {
			Monom monom_nou = new Monom(m1.getGrad(), 0);

			monom_nou.setCoeficient(m1.getCoeficient() * m1.getGrad());
			monom_nou.setGrad(monom_nou.getGrad() - 1);

			derivat.adaugaMonominPolinom(monom_nou);
		}
		derivat = derivat.aranjeaza();
		derivat.sorteaza();
		return derivat;
	}

	public Polinom integrare() { // Metoda de integrare a 2 polinoame

		Polinom m = this;
		Polinom integrat = new Polinom(m.getGrad() + 1);

		for (Monom m1 : m.monoame) {
			MonomDouble monom_nou = new MonomDouble(m1.getGrad(), 0);

			monom_nou.setCoeficient(((double) m1.getCoeficient() / (m1.getGrad() + 1)));
			monom_nou.setGrad(m1.getGrad() + 1);
			integrat.adaugaMonominPolinomDouble(monom_nou);
		}
		return integrat;
	}

	public Polinom inmultire(Polinom p) { // Metoda de inmultire a 2 polinoame

		Polinom m = this;
		Polinom inmultit = new Polinom(m.getGrad() + p.getGrad());

		for (Monom m1 : m.monoame)
			for (Monom m2 : p.monoame) {
				Monom monom_nou = new Monom(m1.getGrad() + m2.getGrad(), 0);

				if (m1.getCoeficient() == 0 || m2.getCoeficient() == 0)
					continue;
				else {
					monom_nou.setCoeficient(m1.getCoeficient() * m2.getCoeficient());

					inmultit.adaugaMonominPolinom(monom_nou);
				}
			}
		
		inmultit =inmultit.aranjeaza();
		inmultit.sorteaza();
		return inmultit;
	}

	public Polinom aranjeaza() { // Metoda care aduna coeficientii monoamelor de
									// acelasi grad dintr-un polinom

		Polinom rezultat = new Polinom();

		for (int i = this.getGrad(); i >= 0; i--) {
			int coeficient = 0;
			Monom mon = new Monom(i, 0);
			for (Monom m : this.monoame) {
				if (m.getGrad() == i) {
					coeficient += m.getCoeficient();
				}
			}
			mon.setCoeficient(coeficient);
			rezultat.monoame.add(mon);
		}
		return rezultat;
	}

	public ArrayList<Polinom> impartire(Polinom p) throws Exception {// Metoda
																		// de
																		// impartire
																		// a 2
																		// polinoame

		ArrayList<Polinom> polinoame = new ArrayList<Polinom>();
		Polinom cat = new Polinom(this.getGrad());
		Polinom rest = new Polinom(this.getGrad());
		rest = this;

		if (p.maxMonom().getCoeficient() == 0 && p.getGrad() == 0) {
			throw new Exception("Impartire cu 0!");
		}

		else if (rest.getGrad() < p.getGrad()) {
			throw new Exception("Grad Polinom1 < Grad Polinom2");

		} else {

			while (rest.getGrad() >= p.getGrad()) {

				Monom dm = rest.maxMonom();
				Monom im = p.maxMonom();	
				Monom m = dm.impartire(im);
					
				cat.monoame.add(m);
				Polinom p1 = new Polinom(m.getGrad() + p.getGrad());
				Polinom proba = new Polinom(m.getGrad());
				proba.adaugaMonominPolinom(m);
				p1 = p.inmultire(proba);
				p1.completeaza_inferior();
				p1.sorteaza();
				rest = rest.scadere(p1);
				rest.sorteaza();
				proba.monoame.remove(0);

				if (rest.maxMonom().getGrad() == p.maxMonom().getGrad()) {

					if (Math.abs(p.maxMonom().getCoeficient()) > Math.abs(rest.maxMonom().getCoeficient())) {
						polinoame.add(cat);
						polinoame.add(rest);
						return polinoame;
					}
				}

			}
		}
		if (rest.monoame.isEmpty()) {
			rest.monoame.add(new Monom(rest.getGrad(), 0));
		}

		polinoame.add(cat);
		polinoame.add(rest);

		return polinoame;

	}

	public Monom maxMonom() { // Metoda care returneaza monomul maxim dintr-un
								// polinom

		Monom m1 = new Monom();
		int max = 0;

		for (Monom m : monoame) {
			if (m.getGrad() >= max && m.getCoeficient() != 0) {
				max = m.getGrad();
				m1.setCoeficient(m.getCoeficient());
			}
		}
		m1.setGrad(max);

		return m1;
	}

	public String showPolinom() { // Metoda de afisare a unui Polinom

		String rezAfisare = new String("");
		for (Monom m : this.monoame) {
			rezAfisare += m.toString();
		}

		for (MonomDouble m1 : this.monoame_double) {
			rezAfisare += m1.toString();
		}
		return rezAfisare;
	}

	public int getGrad() {// Metoda care returneaza gradul unui Polinom, dupa
							// anumite operatii

		int grad1 = 0;
		for (Monom m : this.monoame) {
			if (m.getCoeficient() != 0) {
				if (m.getGrad() >= grad1) {
					grad1 = m.getGrad();
				}
			}
		}
		return grad1;
	}

	public int getGradMax(Polinom p) { // Metoda care returneaza gradul maxim
										// dintre 2 polinoame

		if (p.gradPolinom >= this.gradPolinom)
			return p.gradPolinom;
		else
			return this.gradPolinom;
	}

	public void sorteaza() { // Metoda de sortare descrescatoare a monoamelor
								// unui Polinom
		Comparator<Monom> c = (s1, s2) -> Integer.compare(s2.getGrad(), s1.getGrad());
		this.monoame.sort(c);
	}
	
	public String checkEmpty(){ // Metoda care verifica daca un polinom are monoame de coeficient 0
		boolean ok = true;
		String rezultat = new String("0");
		for(Monom m : this.monoame){
			if(m.getCoeficient() != 0)
				ok = false;
		}
		if(ok == true)
			return rezultat;
		else
			return "";
	}
}