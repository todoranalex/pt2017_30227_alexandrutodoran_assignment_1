import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Controller {

	private View view;

	public Controller() {
	};

	public Polinom prelucrare(String inputString) {

		int coef;
		int grad;

		Polinom p = new Polinom(0);

		inputString = inputString.replaceAll("-", "+-"); // - -> +-
		inputString = inputString.replaceAll(Pattern.quote("^"), "");// scoate ^
		
		

		String[] parts = inputString.split("[+]"); // split la +
		

		for (String nou : parts) {
			
			if	(! nou.equals( "")){ // sare peste primul spatiu, daca ii dam primu caracter "-"
			
			
			String[] splitted = nou.split("[x]"); // -> split la x
			
				
			
			coef = Integer.parseInt(splitted[0]);
			grad = Integer.parseInt(splitted[1]);

			Monom monom_nou = new Monom(grad, coef);
			p.adaugaMonominPolinom(monom_nou);

				}
			else continue;
			}
		
		return p;
	}

	public Controller(View view) {

		this.view = view;
		view.attachDerivare1(new AddListenerDeriv1());
		view.attachDerivare2(new AddListenerDeriv2());
		view.attachIntegrare1(new AddListenerInt1());
		view.attachIntegrare2(new AddListenerInt2());
		view.attachAdaugare(new AddListenerAdaug());
		view.attachScadere(new AddListenerScad());
		view.attachInmultire(new AddListenerInm());
		view.attachImpartire(new AddListenerImpartire());
		view.attachClear(new AddListenerClear());
	}

	class AddListenerClear implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			view.setRezultat("");
			view.setRezultat_rest("");
		}
	}

	class AddListenerDeriv1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String rezultat = "Derivare P1 : ";
			String s = view.getPolinom1();
			Polinom p = new Polinom(0);
			p = prelucrare(s);
			p = p.derivare();

			rezultat = rezultat + p.showPolinom();
			rezultat = rezultat + p.checkEmpty();
			view.setRezultat(rezultat);
		}
	}

	class AddListenerDeriv2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String rezultat = "Derivare P2 : ";
			String s = view.getPolinom2();
			Polinom p = new Polinom(0);
			p = prelucrare(s);
			p = p.derivare();

			rezultat = rezultat + p.showPolinom();
			rezultat = rezultat + p.checkEmpty();
			view.setRezultat(rezultat);

		}
	}

	class AddListenerInt1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String rezultat = "Integrare P1 : ";
			String s = view.getPolinom1();
			Polinom p = new Polinom(0);
			p = prelucrare(s);
			p = p.integrare();
			rezultat = rezultat + p.showPolinom() + "+C";

			view.setRezultat(rezultat);
		}
	}

	class AddListenerInt2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String rezultat = "Integrare P2 : ";
			String s = view.getPolinom2();
			Polinom p = new Polinom(0);
			p = prelucrare(s);
			p = p.integrare();

			rezultat = rezultat + p.showPolinom() + "+C";
			view.setRezultat(rezultat);
		}
	}

	class AddListenerAdaug implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String rezultat = "P1 + P2 = ";
			String s1 = view.getPolinom1();
			String s2 = view.getPolinom2();
			Polinom p1 = new Polinom(0);
			Polinom p2 = new Polinom(0);
			p1 = prelucrare(s1);
			p2 = prelucrare(s2);
			p1.completeaza_inferior();
			p2.completeaza_inferior();
			p1.completeaza_superior(p2);

			Polinom suma = p1.adunare(p2);

			rezultat = rezultat + suma.showPolinom();
			rezultat = rezultat + suma.checkEmpty();
			view.setRezultat(rezultat);
		}
	}

	class AddListenerScad implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String rezultat = "P1 - P2 = ";
			String s1 = view.getPolinom1();
			String s2 = view.getPolinom2();
			Polinom p1 = new Polinom(0);
			Polinom p2 = new Polinom(0);
			p1 = prelucrare(s1);
			p2 = prelucrare(s2);
			p1.completeaza_inferior();
			p2.completeaza_inferior();
			p1.completeaza_superior(p2);

			Polinom diferenta = p1.scadere(p2);

			rezultat = rezultat + diferenta.showPolinom();
			rezultat = rezultat + diferenta.checkEmpty();
			view.setRezultat(rezultat);
		}
	}

	class AddListenerInm implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String rezultat = new String("P1 x P2 = ");
			String s1 = view.getPolinom1();
			String s2 = view.getPolinom2();
			Polinom p1 = new Polinom(0);
			Polinom p2 = new Polinom(0);

			p1 = prelucrare(s1);
			p2 = prelucrare(s2);

			Polinom inmultire = p1.inmultire(p2);

			rezultat = rezultat + inmultire.showPolinom();
			rezultat = rezultat + inmultire.checkEmpty();
			view.setRezultat(rezultat);
		}
	}

	class AddListenerImpartire implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String s1 = view.getPolinom1();
			String s2 = view.getPolinom2();
			Polinom p1 = new Polinom(0);
			Polinom p2 = new Polinom(0);

			p1 = prelucrare(s1);
			p2 = prelucrare(s2);
			p1.completeaza_inferior();
			p2.completeaza_inferior();
			p1.completeaza_superior(p2);
			String rezultat = new String("Cat : ");
			String rezultat1 = new String("Rest : ");
			try {
				ArrayList<Polinom> a = p1.impartire(p2);
				rezultat = rezultat + a.get(0).showPolinom();
				rezultat1 = rezultat1 + a.get(1).showPolinom();
				
				rezultat = rezultat + a.get(0).checkEmpty();
				rezultat1 = rezultat1 + a.get(1).checkEmpty();

			} catch (Exception exception) {
				rezultat = exception.getMessage();
				rezultat1 = "";

			}

			view.setRezultat(rezultat);
			view.setRezultat_rest(rezultat1);
		}
	}
}
