import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.*;

public class View extends JFrame {
	public static final long serialVersionUID = 1L;
	private JPanel main = new JPanel();
	private JTextField polinom1 = new JTextField();
	private JTextField polinom2 = new JTextField();

	private JLabel lblPolinom1 = new JLabel("Polinom 1 :");
	private JLabel lblPolinom2 = new JLabel("Polinom 2 :");
	private JLabel lblRezultat = new JLabel("Rezultat :");
	private JLabel lblPolinom_rest = new JLabel("");
	private JLabel lblPolinom = new JLabel("");

	private JButton adaugare = new JButton("P1 \u002B P2");
	private JButton scadere = new JButton("P1 \u2014 P2");
	private JButton inmultire = new JButton("P1 \u2717 P2 ");
	private JButton impartire = new JButton("P1 \u2215 P2");
	private JButton clear = new JButton("\u2612");

	private JButton derivare1 = new JButton("\u2202 P1");
	private JButton integrare1 = new JButton("\u222B P1");

	private JButton derivare2 = new JButton("\u2202 P2");
	private JButton integrare2 = new JButton("\u222B P2");

	private void putComponents() {
		int buttonWidth = 90;

		lblPolinom1.setBounds(10, 10, 80, 20);
		lblPolinom2.setBounds(10, 80, 80, 20);
		lblRezultat.setBounds(10, 190, 100, 20);
		lblPolinom.setBounds(10, 210, 500, 20); // Pol principal de afisat
		lblPolinom_rest.setBounds(10, 245, 500, 20); // pt rest
		

		polinom1.setBounds(lblPolinom1.getWidth() + 10, 10, 300, 20);
		polinom2.setBounds(lblPolinom2.getWidth() + 10, 80, 300, 20);

		derivare1.setBounds(120, 40, buttonWidth, 30);
		integrare1.setBounds(derivare1.getX() + derivare1.getWidth() + 10, 40, buttonWidth, 30);
		derivare2.setBounds(120, 110, buttonWidth, 30);
		integrare2.setBounds(derivare2.getX() + derivare2.getWidth() + 10, 110, buttonWidth, 30);

		adaugare.setBounds(10, 155, buttonWidth, 30);
		scadere.setBounds(adaugare.getX() + adaugare.getWidth() + 20, 155, buttonWidth, 30); 
		inmultire.setBounds(scadere.getX() + scadere.getWidth() + 10, 155, buttonWidth, 30);
		impartire.setBounds(inmultire.getX() + inmultire.getWidth() + 10, 155, buttonWidth, 30);
		
		clear.setBounds(impartire.getX() + impartire.getWidth() + 10, 155, buttonWidth, 30);

		main.setBackground(Color.LIGHT_GRAY);

		
		main.add(lblPolinom1);
		main.add(lblPolinom2);
		main.add(lblRezultat);
		main.add(lblPolinom_rest);
		main.add(lblPolinom);
		main.add(polinom1);
		main.add(polinom2);
		main.add(derivare1);
		main.add(integrare1);
		main.add(derivare2);
		main.add(integrare2);
		main.add(clear);

		main.add(adaugare);
		main.add(scadere);
		main.add(inmultire);
		main.add(impartire);
	}

	private void jFrameSetup() {
		setSize(550, 300);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public View() {
		add(main);
		main.setLayout(null);
		putComponents();
		jFrameSetup();
	}

	public String getPolinom1() {
		return polinom1.getText();
	}

	public String getPolinom2() {
		return polinom2.getText();
	}

	public void setRezultat(String s) {
		lblPolinom.setText(s);
	}
	
	public void setRezultat_rest(String s) {
		lblPolinom_rest.setText(s);
	}

	public void attachDerivare1(ActionListener a) {
		derivare1.addActionListener(a);
	}

	public void attachDerivare2(ActionListener a) {
		derivare2.addActionListener(a);
	}

	public void attachIntegrare1(ActionListener a) {
		integrare1.addActionListener(a);
	}

	public void attachIntegrare2(ActionListener a) {
		integrare2.addActionListener(a);
	}

	public void attachAdaugare(ActionListener a) {
		adaugare.addActionListener(a);
	}

	public void attachScadere(ActionListener a) {
		scadere.addActionListener(a);
	}

	public void attachInmultire(ActionListener a) {
		inmultire.addActionListener(a);
	}

	public void attachImpartire(ActionListener a) {
		impartire.addActionListener(a);
	}
	public void attachClear(ActionListener a){
		clear.addActionListener(a);
	}
}
