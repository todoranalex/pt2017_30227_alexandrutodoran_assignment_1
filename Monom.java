
public class Monom extends Number {

	public static final long serialVersionUID = 1L;
	private int grad;
	private int coeficient;
	private Number number;

	public Monom() {
		this(0, 0);
	}

	public Monom(Number grad, Number coeficient) {
		this.grad = grad.intValue();
		this.coeficient = coeficient.intValue();
	}

	public int intValue() { // extindere Number
		return number.intValue();
	}

	public double doubleValue() { // extindere Number
		return number.doubleValue();
	}

	public float floatValue() { // extindere Number
		return number.floatValue();
	}

	public long longValue() { //extindere Number
		return number.longValue();
	}

	public int getCoeficient() {
		
		return coeficient;
	}

	public int getGrad() {

		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	public void setCoeficient(int coeficient) {
		this.coeficient = coeficient;
	}

	public String toString() {// Metoda de afisare a unui monom

		if (this.coeficient != 0) {
			if (this.coeficient >= 0) {
				return String.format("+ %d*x^%d ", this.coeficient, this.getGrad());

			} else if (this.coeficient < 0) {
				return String.format("- %d*x^%d ", -this.coeficient, this.getGrad());

			}
		}
		return "";
	}

	public int getGradMax(Monom p) {// Metoda care returneaza gradul
									// maxim al unui Monom

		if (p.getGrad() >= this.getGrad())
			return p.grad;
		else
			return this.grad;
	}

	public Monom impartire(Monom p) {// Metoda care returneaza rezultatul
										// impartirii a 2 monoame

		Monom m = this;
		Monom rezultat = new Monom(m.grad - p.grad, 0);
		rezultat.setCoeficient(m.getCoeficient() / p.getCoeficient());
		rezultat.setGrad(m.getGrad() - p.getGrad());

		return rezultat;
	}

}
